# feature 7.2.0:
# imported redirect
from django.shortcuts import render, redirect

# feature 7.2.0:
# imported redirect, login, the view model from forms.py (LoginForm)
# feature 10.2:
# imported SignUpForm Model
from accounts.forms import LoginForm, SignUpForm

# feature 9.1.0: importing logout
from django.contrib.auth import login, authenticate, logout

# feature 10.2.0:
# imported User
from django.contrib.auth.models import User

# Create your views here.


# feature 7.2:
# creating a view function for LoginForm
def user_login(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            # feature 7.3
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)

                # can the redirect also be list_projects?
                # or no because its in a separate file
                return redirect("home")
    else:
        form = LoginForm()

    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


# feature 9.1:
# create view for logout which redirects us to the login page
def user_logout(request):
    logout(request)
    return redirect("login")


# feature 10.2:
# creating a view for a user to sign up(why isnt sign up
# first and then log in made? is it because we are log in as a superuser?)
def user_signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]
            # feature 10.3
            if password == password_confirmation:
                # feature 10.5
                user = User.objects.create_user(
                    username=username, password=password
                )
                # feature 10.6: login user if 'password' and
                # 'password_confirmation' match
                login(request, user)
                # feautre 10.4: redirects to their lists if sign up successful
                return redirect("list_projects")
            else:
                # feature +10.3: adds an error if 'password' and
                # password_confirmation' do not match
                form.add_error(
                    "password", "Passwords not match, please try again."
                )

    else:
        form = SignUpForm()

    context = {
        "form": form,
    }
    return render(request, "accounts/signup.html", context)
