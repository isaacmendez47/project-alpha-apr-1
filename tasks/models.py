# feature 11.0: we import models from the djano db
# and 'User', and 'Project" model from the app 'projects
from django.db import models
from django.contrib.auth.models import User
from projects.models import Project


# feature 11: Task model
class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField()
    due_date = models.DateTimeField()
    is_completed = models.BooleanField(default=False)
    project = models.ForeignKey(
        Project, related_name="tasks", on_delete=models.CASCADE
    )
    assignee = models.ForeignKey(
        User, related_name="tasks", on_delete=models.CASCADE, null=True
    )

    def __str__(self):
        return self.name
