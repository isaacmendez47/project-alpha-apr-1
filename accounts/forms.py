# feature 7.1.0: created this ifle.
# we import a model called forms from django so we can
# base this login feature off of it
from django import forms


# feature 7.1:
class LoginForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput)


# feature 10.1: creating a form for user sign ups
class SignUpForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput)
    password_confirmation = forms.CharField(
        max_length=150, widget=forms.PasswordInput
    )
