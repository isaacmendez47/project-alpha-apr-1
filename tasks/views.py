# feature 15.1.0: imported redirect
from django.shortcuts import render, redirect

# feature 15.1.0: import modelform(TaskForm) and login decorator
from tasks.forms import TaskForm
from django.contrib.auth.decorators import login_required

# feature 16.1.0: imported Task model
from tasks.models import Task

# Create your views here.


# feature 15.1: created a create view that shows form to
# create an instance of the task model with all
# properties(except is_completed)
# feature 15.3: protected the view
@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            # feature 15.2: 'form.save()' not 'form.save(False)'
            form.save()
            # feature 15.4: redirects to list of projects
            # why wouldnt it redirect to the detail of the lists?
            return redirect("list_projects")
    else:
        form = TaskForm()

    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


# feature 16.1-2: created view for tasks
# and protected view
@login_required
def show_tasks(request):
    tasks_lists = Task.objects.filter(assignee=request.user)
    context = {
        "tasks_lists": tasks_lists,
    }
    return render(request, "tasks/list.html", context)
