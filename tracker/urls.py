"""
URL configuration for tracker project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin

# feature 5.3.0: we import the include to use in our path
from django.urls import path, include

# feature 6.0: we import redirect so when we type localhost:8000/
# itll redirect us to the list view
from django.shortcuts import redirect

#

# feature 5.3: registering the urls.py thats in our app.
# the 'include' makes so it any url registered in the app
# is automatically included here


# feature 6: redirects to the list view
def redirect_to_project_lists(request):
    return redirect("list_projects")


# feature 6 : registered the function as a path
# so itll redirect us to the list view of projects

# feature 7.5: registering the app 'accounts' and its urls
# feature 15.6: registered tasks/urls.py file
urlpatterns = [
    path("tasks/", include("tasks.urls")),
    path("accounts/", include("accounts.urls")),
    path("", redirect_to_project_lists, name="home"),
    path("projects/", include("projects.urls")),
    path("admin/", admin.site.urls),
]
