from django.db import models
from django.contrib.auth.models import User


# feature 3:
# creating a model called Projects with foreignkey
# pointed to User. we import the User model
# from django.contrib.auth.models
class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    owner = models.ForeignKey(
        User, related_name="projects", on_delete=models.CASCADE, null=True
    )

    def __str__(self):
        return self.name
